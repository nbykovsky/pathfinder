import unittest, sys
sys.path.append('../')
import mathspace.shortest_path as sp


class TestShortestPathParts(unittest.TestCase):

    def test_get_arrow_id_top_left(self):
        trn = sp.Size(3, 2)
        p1 = sp.Point(0, 0)
        p2 = sp.Point(1, 0)
        drn = sp.Directions(True, True, True, True)

        """
              0    1    2
            +----+----+----+
         0  |  --|--> |    |
            +----+----+----+
         1  |    |    |    |  => Block1(shift = 0) && [1, 0, 0, 0, 0, 0] => idx == 0
            +----+----+----+

        """

        arrow_idx1 = sp.get_arrow_idx(sp.Arrow(p1, p2), trn, drn)
        self.assertTrue(arrow_idx1 == 0, 'index of arrow from (0, 0) to (1, 0) should be equal to 0 current value is '
                                         '%s ' % arrow_idx1)

        arrow_idx2 = sp.get_arrow_idx(sp.Arrow(p2, p1), trn, drn)
        self.assertTrue(arrow_idx2 == 7, 'index of arrow from (1, 0) to (0, 0) should be equal to 7 current value is '
                                         '%s ' % arrow_idx2)

    def test_get_arrow_id_bottom_right(self):
        trn = sp.Size(3, 2)
        p1 = sp.Point(2, 0)
        p2 = sp.Point(2, 1)
        drn = sp.Directions(True, True, True, True)
        arrow_idx1 = sp.get_arrow_idx(sp.Arrow(p1, p2), trn, drn)
        self.assertTrue(arrow_idx1 == 6, 'index of arrow from (2, 0) to (2, 1) should be equal to 6 current value is '
                                         '%s ' % arrow_idx1)

        arrow_idx2 = sp.get_arrow_idx(sp.Arrow(p2, p1), trn, drn)
        self.assertTrue(arrow_idx2 == 13, 'index of arrow from (2, 1) to (2, 0) should be equal to 13 current value is '
                                          '%s ' % arrow_idx2)

    def test_get_arrow_id_bottom_right_horiz(self):
        trn = sp.Size(3, 2)
        p1 = sp.Point(1, 1)
        p2 = sp.Point(2, 1)
        drn = sp.Directions(True, True, True, True)
        arrow_idx1 = sp.get_arrow_idx(sp.Arrow(p1, p2), trn, drn)
        self.assertTrue(arrow_idx1 == 3, 'index of arrow from (1, 1) to (2, 1) should be equal to 3 current value is '
                                         '%s ' % arrow_idx1)

        arrow_idx2 = sp.get_arrow_idx(sp.Arrow(p2, p1), trn, drn)
        self.assertTrue(arrow_idx2 == 10, 'index of arrow from (2, 1) to (1, 1) should be equal to 10 current value is '
                                          '%s ' % arrow_idx2)


    def test_get_neighbors_top_left(self):
        """
              0    1    2    3    4
            +----+----+----+----+----+
         0  | X  | +  |    |    |    |
            +----+----+----+----+----+
         1  | +  |    |    |    |    |
            +----+----+----+----+----+
         2  |    |    |    |    |    |
            +----+----+----+----+----+
        """
        drn = sp.Directions(True, True, True, True)
        neighbors = sp.get_neighbors(sp.Point(0, 0), sp.Size(5, 3), drn, [])
        self.assertTrue(sp.Point(1, 0) in neighbors, 'Point(1, 0) should be within neighbors of Point(0, 0)')
        self.assertTrue(sp.Point(0, 1) in neighbors, 'Point(0, 1) should be within neighbors of Point(0, 0)')
        self.assertTrue(len(neighbors) == 2, 'Point(0, 0) should have 2 neighbors')

    def test_get_neighbors_bottom_right(self):
        """
              0    1    2    3    4
            +----+----+----+----+----+
         0  |    |    |    |    |    |
            +----+----+----+----+----+
         1  |    |    |    |    | +  |
            +----+----+----+----+----+
         2  |    |    |    | +  | X  |
            +----+----+----+----+----+
        """
        drn = sp.Directions(True, True, True, True)
        neighbors = sp.get_neighbors(sp.Point(4, 2), sp.Size(5, 3), drn, [])
        self.assertTrue(sp.Point(4, 1) in neighbors, 'Point(4, 1) should be within neighbors of Point(4, 2)')
        self.assertTrue(sp.Point(3, 2) in neighbors, 'Point(3, 2) should be within neighbors of Point(4, 2)')
        self.assertTrue(len(neighbors) == 2, 'Point(4, 2) should have 2 neighbors')

    def test_get_neighbors_middle(self):
        """
              0    1    2    3    4
            +----+----+----+----+----+
         0  |    |    | +  |    |    |
            +----+----+----+----+----+
         1  |    | +  | X  | +  |    |
            +----+----+----+----+----+
         2  |    |    | +  |    |    |
            +----+----+----+----+----+
        """
        drn = sp.Directions(True, True, True, True)
        neighbors = sp.get_neighbors(sp.Point(2, 1), sp.Size(5, 3), drn, [])
        self.assertTrue(sp.Point(2, 0) in neighbors, 'Point(2, 0) should be within neighbors of Point(2, 1)')
        self.assertTrue(sp.Point(3, 1) in neighbors, 'Point(3, 1) should be within neighbors of Point(2, 1)')
        self.assertTrue(sp.Point(2, 2) in neighbors, 'Point(2, 2) should be within neighbors of Point(2, 1)')
        self.assertTrue(sp.Point(1, 1) in neighbors, 'Point(1, 1) should be within neighbors of Point(2, 1)')
        self.assertTrue(len(neighbors) == 4, 'Point(2, 1) should have 4 neighbors')

    def test_get_constraint_top_left(self):
        """
        Out arrows                   In arrows

              0    1    2                 0    1    2
            +----+----+----+            +----+----+----+
         0  |  --|->  |    |         0  | ^<-|-   |    |
            +--|-+----+----+            +-|--+----+----+
         1  |  V |    |    |         1  |    |    |    |
            +----+----+----+            +----+----+----+
        """
        p = sp.Point(0, 0)
        drn = sp.Directions(True, True, True, True)
        ctr = sp.get_constraint(p, sp.Size(3, 2), drn, [])
        z = zip(ctr, [1, 0, 0, 0, 1, 0, 0, -1, 0, 0, 0, -1, 0, 0])
        self.assertTrue(all(map(lambda x: x[0] == x[1], z)), 'top left constraint is wrong')

    def test_get_constraint_bottom_right(self):
        """
        Out arrows                   In arrows

              0    1    2                 0    1    2
            +----+----+----+            +----+----+----+
         0  |    |    |  ^ |         0  |    |    |    |
            +----+----+--|-+            +----+----+--|-+
         1  |    |  <-|--  |         1  |    |  --|->\/|
            +----+----+----+            +----+----+----+
        """
        p = sp.Point(2, 1)
        drn = sp.Directions(True, True, True, True)
        ctr = sp.get_constraint(p, sp.Size(3, 2), drn, [])
        z = zip(ctr, [0, 0, 0, -1, 0, 0, -1, 0, 0, 0, 1, 0, 0, 1])
        self.assertTrue(all(map(lambda x: x[0] == x[1], z)), 'bottom right constraint is wrong')

    def test_get_dummy_objective(self):
        pass

    def test_get_arrow(self):

        trn = sp.Size(5, 5)
        drn = sp.Directions(True, True, True, True)
        arrow = sp.get_arrow(0, trn, drn)
        self.assertTrue(arrow == sp.Arrow(sp.Point(0, 0), sp.Point(1, 0)), 'Arrow is wrong')

    def test_get_arrow1(self):
        trn = sp.Size(2, 2)
        drn = sp.Directions(True, True, False, True)
        arrow = sp.get_arrow(5, trn, drn)
        self.assertTrue(arrow == sp.Arrow(sp.Point(1, 1), sp.Point(1, 0)), 'Arrow is wrong')

    def test_get_arrow_idx_all(self):
        trn = sp.Size(43, 11)

        def run_case(trn, drn):
            arrows = []
            for idx in range(sp.get_vector_size(trn, drn)):
                arrow = sp.get_arrow(idx, trn, drn)
                arrows.append(arrow)
                _idx = sp.get_arrow_idx(arrow, trn, drn)
                self.assertTrue(idx == _idx, 'ids should be %s but it is %s' % (idx, _idx))
            self.assertTrue(len(arrows) == len(set(arrows)), 'probably arrows have duplicates')
        for i in range(16):
            case = list(map(bool, map(int, list("{0:{fill}4b}".format(i, fill='0')))))
            run_case(trn, sp.Directions(*case))

    def test_get_constraint_2x2(self):

        trn = sp.Size(2, 2)
        drn = sp.Directions(True, True, False, True)
        self.assertTrue(sp.get_constraint(sp.Point(0, 0), trn, drn, []) == [1, 0, 1, 0, -1, 0],
                        'Constraint for Point(0, 0) is wrong')

        self.assertTrue(sp.get_constraint(sp.Point(1, 1), trn, drn, []) == [0, -1, 0, -1, 0, 1],
                       'Constraint for Point(1, 1) is wrong')

    def test_get_arrow_idx1(self):
        drn = sp.Directions(True, True, False, True)
        trn = sp.Size(2, 2)
        arrow = sp.Arrow(sp.Point(1, 1), sp.Point(1, 0))
        idx = sp.get_arrow_idx(arrow, trn, drn)
        self.assertTrue(idx == 5, 'idx should be 5 current value is %s ' % idx)

    def test_get_vector_size(self):
        trn = sp.Size(2, 2)
        drn = sp.Directions(True, True, False, True)
        self.assertTrue(sp.get_vector_size(trn, drn) == 6)

    def test_get_shifts(self):
        trn = sp.Size(2, 2)
        drn = sp.Directions(True, True, False, True)
        shifts = sp.get_shifts(trn, drn)
        self.assertTrue(shifts == sp.Directions(0, 2, -1, 4))


def run_test(file_name):
    matrix, start, end, trn, drn, dead, code = sp.read_data(file_name)
    result = sp.solver(matrix, start, end, trn, drn, dead)
    if not result:
        return False
    return [*result]


class TestShortestPath(unittest.TestCase):

    def test_case1_simple(self):
        path, words, cost = run_test('case1.txt')
        self.assertTrue(words == ['down', 'right'])
        self.assertTrue(cost == 19.0)

    def test_case2_one_dead_point(self):
        path, words, cost = run_test('case2.txt')
        self.assertTrue(words == ['right', 'down'])
        self.assertTrue(cost == 20)

    def test_case3_no_path(self):
        result = run_test('case3.txt')
        self.assertFalse(result)

    def test_case4_from_task(self):
        path, words, cost = run_test('case4.txt')
        self.assertTrue(words == ['right', 'right', 'down', 'down', 'right', 'down', 'down', 'right', 'right', 'down'])

    def test_case5_6x6_up_down(self):
        path, words, cost = run_test('case5.txt')
        expected = [ 'down', 'down', 'down', 'down', 'down',
                     'right', 'right',
                     'up', 'up',
                     'right', 'right', 'right',
                     'down', 'down']
        self.assertTrue(words == expected)

    def test_case6_6x6_backward(self):
        path, words, cost = run_test('case6.txt')
        expected = [ 'up', 'up', 'up', 'up', 'up',
                     'left', 'left',
                     'down', 'down',
                     'left', 'left', 'left',
                     'up', 'up']
        self.assertTrue(words == list(reversed(expected)))

    def test_case7_big(self):
        path, words, cost = run_test('case7.txt')
        expected = ["down", "down", "down", "down", "down", "down", "down", "down", "down",
                    "down", "down", "down", "down", "down", "down", "down", "down", "down",
                    "down", "down", "down", "right", "right", "right", "up", "up", "up", "up",
                    "up", "up", "up", "up", "up", "up", "up", "up", "right", "right", "up", "up",
                    "up", "up", "up", "up", "left", "left", "up", "up", "right", "right", "right",
                    "right", "down", "down", "down", "down", "down", "down", "down", "down", "down",
                    "down", "left", "down", "down", "down", "down", "down", "down", "down", "down",
                    "down", "right", "right", "right", "right", "right", "up", "up", "up", "up", "up",
                    "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "up",
                    "up", "right", "right", "right", "right", "right", "right", "right", "right",
                    "right", "right", "right", "right", "down", "down", "down", "down", "down",
                    "down", "down", "down", "down", "down", "down", "down", "down", "down", "down",
                    "down", "down", "down", "left", "left", "left", "up", "up", "up", "up", "up",
                    "up", "up", "up", "up", "up", "up", "up", "up", "up", "up", "left", "left",
                    "left", "left", "left", "left", "down", "down", "down", "left", "down", "down",
                    "down", "down", "down", "right", "down", "down", "right", "right", "right",
                    "down", "down", "down", "down", "left", "left", "down", "down", "down", "down",
                    "right", "right", "right", "right", "right", "right", "right", "right"]
        self.assertTrue(words == expected)

    def test_case8_no_path(self):
        result = run_test('case8.txt')
        self.assertFalse(result)

    def test_case9_r_top_l_bottom(self):
        path, words, cost = run_test('case9.txt')
        expected = [ 'left', 'left',
                     'down', 'down', 'down',
                     'left',
                     'down', 'down',
                     'left', 'left']
        self.assertTrue(words == expected)

    def test_case10_middle_middle(self):
        path, words, cost = run_test('case10.txt')
        expected = ["up", "up", "left", "left", "left", "down", "down", "down", "down", "right", "right", "up"]
        self.assertTrue(words == expected)


if __name__ == '__main__':

    unittest.main()
