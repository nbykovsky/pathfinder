"""Search of the shortest path on terrain 

This program searches for a minimum cost path on terrain between two given points
as input program requires the text file (see ../test/case*.txt files as an example)

Required features:
    * program should find a path between top left and bottom right corners
    * right, bottom moves are initially allowed
    * up, left moves might be added as comprehension  
    
Implemented features:
    * program could find path between any 2 points on terrain 
    (coordinates and start and finish point are the parts of input file)
    * user could allow any combination of moves from (left, right, bottom, down). 
    Moves are part of input file
    * user could mark some points of terrain as dead points, which means that
    this points will not be used in the path
    
How to run the program: 
    "python3 shortest_path.py /path/to/test/case.txt"
    
Information:
    "python3 shortest_path.py"
    
Requirements:
    see requirements.txt file
    
Algorithm:
    Obviously terrain with moves (left, right, up, down) could be considered as directed graph with weights on each
    edge. Where cell of terrain is represented by Vertex and move between two cells could be represented by Edge
    of graph. Weight of edge is value in cell towards which move is done (for example if we have Edge from
    cell A to cell B, the weight of this Edge will be value of cell B )

    The classical solution of this problem would be the Dijkstra's algorithm https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
    of finding the shortest path on graph. However I decided to solve this task as a linear optimization problem

    The basic idea of this program is the following
    1. For each point of terrain we find all the arrows from this point and to this point
    2. We code each arrow as on integer number idx (from 0 to number of arrows)
    3. We build constraint vector for point. For each arrow going out of point we put 1 in idx place,
    each arrow coming into point we put -1. 
    4. We build column for tableau (1 for start, -1 for finish, 0 for end)
    5. For each arrow we put value into objective function 
    6. Solve problem using scipy.optimize.linprog
    7. Transform Optimized vector back into path of arrows
    
    
Input file should have the following format:
         1 row - two integers greater then zero separated by space (X,Y sizes of terrain)
         2 row - allowed moves - from 1 to 4 chars from the set (r,d,u,l) separated
                r - right move
                d - down move
                u - up move
                l - left move
         3 row - two integers separated by space - coordinates of start point of path
                (index starts from 0, point should be within terrain defines in row 1)
         4 row - two integers separated by space - coordinates of end point of path
                (index starts from 0, point should be within terrain defines in row 1)
         5 row - type of numbers in matrix ('hex' - for hex grid, 'dec' - for dec grid)
         6+ row - starting from this row matrix of weights should be presented
                each row of matrix represents by X non negative hex numbers separated by space,
                where X is the first integer from the row 1. The number of rows in matrix
                should be equal to the second integer in the row 1. Algorithm supports dead points
                (points which must be used in path), such points should be marked as "---"
                
          
         Example of input file:
         -----------------------
         6 6
         r d u l
         0 0
         5 5
         hex
         46B E59 EA --- 45E 63
         899 FFF 926 7AD C4E FFF
         E2E --- 6D2 976 83F C96
         9E9 A8B 9C1 461 F74 D05
         EDD E94 5F4 D1D D03 DE3
         89 925 CF9 CA0 F18 4D2
         ----------------------
         
         Example of output:
         -------------------------
         Minimum cost is: 23015.0 
         Optimal path is:
         [46b] [e59] [ea ]  ---   45e   63   
          899   fff  [926]  7ad   c4e   fff  
          e2e   ---  [6d2] [976]  83f   c96  
          9e9   a8b   9c1  [461]  f74   d05  
          edd   e94   5f4  [d1d] [d03] [de3] 
          89    925   cf9   ca0   f18  [4d2] 
         right -> right -> down -> down -> right -> down -> down -> right -> right -> down
         --------------------------------
         Point which where selected for path are wrapped into brackets

    

Author:
     Nikolay Bykovsky <nikolay.bykovsky@gmail.com>

"""

import sys
from typing import List, Tuple, Union

from collections import namedtuple
from scipy.optimize import linprog
from functools import reduce

# Point in terrain
Point = namedtuple('Point', ['x', 'y'])
# Arrow between two points
Arrow = namedtuple('Arrow', ['frm', 'to'])
# Size of terrain
Size = namedtuple('Size', ['X', 'Y'])
# This tuple is used to store different content related to moves
Directions = namedtuple('Directions', ['right', 'down', 'left', 'up'])

# Since we decided to build an optimization problem, we need to be able to uniquely map each Arrow (move between
# cells) to vector (with format [0, 0, 0 .... 1, ....,0]). To do that we split vector to blocks each block will be
# used for specific type of Arrow.
# For example in terrain 2 X 2 there are 8 Arrows in total so we will get following blocks:
# block 1 (right) from 0 to 2: [1, 0, 0, 0, 0, 0, 0, 0], [0, 1, 0, 0, 0, 0, 0, 0]
# block 2 (down)  from 2 to 4: [0, 0, 1, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0, 0]
# block 2 (left)  from 4 to 6: [0, 0, 0, 0, 1, 0, 0, 0], [0, 0, 0, 0, 0, 1, 0, 0]
# block 2 (up)    from 6 to 8: [0, 0, 0, 0, 0, 0, 1, 0], [0, 0, 0, 0, 0, 0, 0, 1]
# if not all the moves are allowed, number of blocks is reduced


def get_2d_sizes(trn: Size) -> Directions:
    """Sizes of blocks
        :param trn: size of terrain
        :return: tuple, where each element is pair of horizontal and vertical sizes
    """
    horizontal = ((trn.X - 1), trn.Y)
    vertical = (trn.X, (trn.Y - 1))
    return Directions(horizontal, vertical, horizontal, vertical)


def get_blocks_sizes(trn: Size) -> Directions:
    """Sizes of blocks in vector
    :param trn: size of terrain
    :return:  tuple, where each element is a number of Arrows of corresponding direction
    """
    return Directions(*map(lambda a: a[0] * a[1], get_2d_sizes(trn)))


def get_shifts(trn: Size, drn: Directions) -> Directions:
    """Shifts of each block within vector 
    :param trn: sizes of terrain
    :param drn: allowed dirrections
    :return: Directions tuple with shifts of each block, if move is not allowed this position will be filled by -1
    """
    shifts = reduce(
        lambda acc, v: (acc[0] + v[0] if v[1] else acc[0], acc[1] + [acc[0]] if v[1] else acc[1] + [-1]),
        zip(get_blocks_sizes(trn), drn),
        (0, []))
    return Directions(*shifts[1])


def get_vector_size(trn: Size, drn: Directions) -> int:
    """Calculates size of vector (constraint in tableau matrix)
    :param trn: sizes of terrain
    :param drn: allowed dirrections
    :return: 
    """
    return sum([t for t, d in zip(get_blocks_sizes(trn), drn) if d])


# these are conditions which help to recognise the direction of Arrow
# only one lambda in tuple could produce True other will produce False
conditions = Directions(lambda arr: arr.frm.x < arr.to.x, lambda arr: arr.frm.y < arr.to.y,
                        lambda arr: arr.frm.x > arr.to.x, lambda arr: arr.frm.y > arr.to.y)


def get_arrow_idx(arrow: Arrow, trn: Size, drn: Directions) -> int:
    """Calculates the index of arrow within vector (constraint)
    :param arrow: pair with from point, to point (see on the top)
    :param trn: sizes of terrain
    :param drn: allowed directions
    :return: 
    """
    # this is helper method which produces coefficients for corresponding directions
    def get_coeffs(arrow):
        return Directions((arrow.frm.y, arrow.frm.x), (arrow.frm.y, arrow.frm.x),
                          (arrow.to.y, arrow.to.x), (arrow.to.y, arrow.to.x))
    # the general idea behind the block bellow is the following
    # index = shift + Y * terrainWidth + X;
    # since Arrow has 2 points and terrainWidth could be different for different directions of arrow
    # we have several cases which are covered by the rows bellow. Each row has [0] at the end, in all cases
    # this lists will have 1 value (even without [0])
    multiplier = [size[0] for size, condition in zip(get_2d_sizes(trn), conditions) if condition(arrow)][0]
    shift = [sft for sft, condition in zip(get_shifts(trn, drn), conditions) if condition(arrow)][0]
    coeffs = [cfs for cfs, condition in zip(get_coeffs(arrow), conditions) if condition(arrow)][0]
    return shift + coeffs[0] * multiplier + coeffs[1]


def get_neighbors(point: Point, trn: Size, drn: Directions, dead: List[Point]) ->List[Point]:
    """Finds the neighboring point for current point
    :param point: point ti which we want to find neighbors(
    :param trn: sizes of terrain
    :param drn: allowed directions
    :param dead: list if points which visiting is not allowed
    :return: neighbors of point (not more the 4)
    """
    # shifts of neighbors
    moves = Directions((1, 0), (0, 1), (-1, 0), (0, -1))
    neighbors = [Point(point.x + x, point.y + y) for (x, y) in moves
                 if 0 <= point.x + x < trn.X and 0 <= point.y + y < trn.Y  # we want neighbor with in terrain
                 and Point(point.x + x, point.y + y) not in dead]  # we want neighbor is not dead
    return neighbors


def get_constraint(point: Point, trn: Size, drn: Directions, dead: List[Point]) -> List[int]:
    """Get constraint of tableau specific for point
    :param point: point to which we are looking for constraint
    :param trn: sizes of terrain
    :param drn: allowed directions
    :param dead: list if points which visiting is not allowed
    :return: vector of size - the number of all arrows
    """

    # arrow might be to allowed by configuration
    def is_arrow_allowed(arrow):
        return any([d and cnd(arrow) for d, cnd in zip(drn, conditions)])

    neighbors = get_neighbors(point, trn, drn, dead)
    # arrows which are coming from neighbor to point
    in_arrows = [get_arrow_idx(Arrow(nbr, point), trn, drn) for nbr in neighbors if is_arrow_allowed(Arrow(nbr, point))]
    # arrows which are going from pint to outside
    out_arrows = [get_arrow_idx(Arrow(point, nbr), trn, drn) for nbr in neighbors if is_arrow_allowed(Arrow(point, nbr))]
    # we include in arrows with 1 out arrow with -1. Finally all constraints except first and last will be included into
    # tableau with == 0
    return [1 if i in out_arrows else -1 if i in in_arrows else 0
            for i in range(get_vector_size(trn, drn))]


def get_dummy_objective(vector: List[int], point: Point, value: int, trn: Size, drn: Directions, dead: List[Point]) -> List[int]:
    """Applies ons point to existing objective function
    :param vector: objective function
    :param point: current point
    :param value: value of current point in weights matrix
    :param trn: sizes of terrain
    :param drn: allowed directions
    :param dead: list if points which visiting is not allowed
    :return: the new version of objective function
    """
    # if point with in dead points, don't apply anything
    if point in dead:
        return vector
    neighbors = get_neighbors(point, trn, drn, dead)
    # weight of each arrow is equal to second point (destination)
    # that's wny we consider only in arrows
    in_arrows = [get_arrow_idx(Arrow(nbr, point), trn, drn) for nbr in neighbors]
    # for each index from in_arrows we apply the new value for the rest of elements we keep old value
    # value should override only 0 (there shouldn't be overlaps)
    return [value if i in in_arrows else v for i, v in enumerate(vector)]


def get_problem(matrix: List[List[int]], p_start: Point, p_finish: Point, trn:Size, drn: Directions,
                dead: List[Point]) -> Tuple[List[List[int]], List[int], List[int]]:
    """Builds linear programming problem
    :param matrix: input terrain
    :param p_start: start point
    :param p_finish: end point
    :param trn: sizes of terrain
    :param drn: allowed directions 
    :param dead: list if points which visiting is not allowed
    :return: tuple of 3 values (tableau)
        1. Matrix of constraints
        2. Right hand side column of constrain (relations between matrix and column is ==)
        3. Objective function
    """
    _pnt_lst = [[Point(x, y) for x in range(trn.X)] for y in range(trn.Y)]

    # list all the points from terrain into
    points_list = reduce(lambda acc, x: acc + x, _pnt_lst, [])
    # list all the values from matrix into flat array
    value_list = reduce(lambda acc, x: acc + x, matrix, [])
    # for each not dead point calculate constraint
    constraints = [get_constraint(point, trn, drn, dead) for point in points_list if point not in dead]
    # takes zero vector [0, 0 ...] as initial value and apply get_dummy_objective many times
    objective = reduce(lambda vector, pv: get_dummy_objective(vector, pv[0], pv[1], trn, drn, dead),
                       zip(points_list, value_list),
                       [0] * get_vector_size(trn, drn))
    # start point has +1 in constraint because the number of out arrows should be to 1 higher then
    # number of in arrows
    # end point has -1 because number of in arrows should be to 1 lower then
    # number of out arrows
    # all other points have 0 in constraint because number of in arrows must be equal to number of out arrows
    column = [1 if point == p_start else -1 if point == p_finish else 0 for point in points_list if point not in dead]
    return list(constraints), column, objective


def solve_problem(constraints: List[List[int]], column: List[int], objective: List[int]):
    """Solves linear programming problem (minimizes objective function)
    :param constraints: Matrix of constraints
    :param column: Right hand side column of constrain (relations between matrix and column is ==)
    :param objective: Objective function
    :return: scipy solution
    """
    return linprog(objective, A_eq=constraints, b_eq=column)


def get_arrow(idx: int, trn: Size, drn: Directions) -> Arrow:
    """Decodes arrow index into Arrow
    :param idx: index of arrow
    :param trn: sizes of terrain
    :param drn: allowed directions 
    :return: Arrow
    """
    # maps reference point to arrow (separate to each direction)
    arrow_decode = Directions(lambda p: Arrow(p, Point(p.x + 1, p.y)), lambda p: Arrow(p, Point(p.x, p.y + 1)),
                              lambda p: Arrow(Point(p.x + 1, p.y), p), lambda p: Arrow(Point(p.x, p.y + 1), p))

    #small helper because divmod return values in wrong order
    def swap(a, b): return Point(b, a)
    return [decode(swap(*divmod(idx - shift, size[0])))  # basically here we de transformation opposite to get_arrow_idx
            for shift, size, decode in zip(list(get_shifts(trn, drn)), get_2d_sizes(trn), arrow_decode)
            if 0 <= shift <= idx][-1]  # here the trick we take the last element where shift <= idx
                                       # it works lise select max(shift) from .. where shift < idx


def get_path(vector: List[int], p_start: Point, trn: Size, drn: Size) -> List[Arrow]:
    """Calculate path from result of optimisation
    :param vector: vector of values after optimisation (due to the specific of problem all the values are binary)
    :param p_start: starting point
    :param trn: sizes of terrain
    :param drn: allowed directions 
    :return: 
    """
    # decodes each index of vector which value is 1
    path = [get_arrow(i, trn, drn) for (i, v) in enumerate(vector) if v == 1]
    sorted_path, start = [], p_start
    # now we have a bunch of arrow but it's hard to recognize the bath because they are not sorted
    # sorting in not elegant but it's just and output. Despite it's complexity is n^2, complexity
    # of all algorithm is limited by complexity of optimization
    for i in range(len(path)):
        nxt = list(filter(lambda p: p.frm == start, path))[0]
        sorted_path.append(nxt)
        start = nxt.to
    return sorted_path


def path_to_words(path: List[Arrow]) -> List[str]:
    """Transforms path to readable format
    :param path: path from Arrows
    :return: path from string (up, down, left, right)
    """
    def helper(arrow): return [f for f, c in zip(Directions._fields, conditions) if c(arrow)][0]
    return [helper(arrow) for arrow in path]


def solver(matrix: List[List[int]], p_start: Point, p_finish: Point, trn:Size, drn: Directions,
           dead: List[Point]) -> Union[Tuple[List[Arrow], List[str], int], bool]:
    """Searches optimal path between points
    :param matrix: - matrix of weights
    :param p_start: start point
    :param p_finish: end point
    :param trn: sizes of terrain
    :param drn: allowed directions 
    :param dead: list if points which visiting is not allowed
    :return: False if there is not feasible solution
            tuple of 3 values otherwise
            1. Path from Arrows
            2. Path from words
            3. Minimum cost
    """
    # build tableau
    problem = get_problem(matrix, p_start, p_finish, trn, drn, dead)
    # solve tableau
    solution = solve_problem(*problem)
    if not solution.success:
        return False
    path = get_path(solution.x, p_start, trn, drn)
    words = path_to_words(path)
    return path, words, solution.fun


def validate_input(matrix, start, end, trn, drn, dead, code):
    err = []
    if trn.X <= 0 or trn.Y <= 0:
        err.append('ERROR: each dimension of terrain should be greater than 0')
    if any([len(r) != trn.X for r in matrix]) or len(matrix) != trn.Y:
        err.append('ERROR: terrain has wrong format')
    if any([any([p > t for p, t in zip(point, trn)]) for point in [start, end]]):
        err.append('ERROR: start or end point is not inside terrain')
    if start in dead or end in dead:
        err.append('ERROR: path shouldn\'t start or end in dead point ("---") ')
    if not all([all([v >= 0 for v in r if v != -1]) for r in matrix]):
        err.append('ERROR: all the values of matrix must be not negative')

    if err:
        print('\n'.join(err))
        print_help()
        sys.exit(0)


def print_help():
    msg = '''Input file should have the following format:
         1 row - two integers greater then zero separated by space (X,Y sizes of terrain)
         2 row - allowed moves - from 1 to 4 chars from the set (r,d,u,l) separated
                r - right move
                d - down move
                u - up move
                l - left move
         3 row - two integers separated by space - coordinates of start point of path
                (index starts from 0, point should be within terrain defines in row 1)
         4 row - two integers separated by space - coordinates of end point of path
                (index starts from 0, point should be within terrain defines in row 1)
         5 row - type of numbers in matrix ('hex' - for hex grid, 'dec' - for dec grid)
         6+ row - starting from this row matrix of weights should be presented
                each row of matrix represents by X non negative hex numbers separated by space,
                where X is the first integer from the row 1. The number of rows in matrix
                should be equal to the second integer in the row 1. Algorithm supports dead points
                (points which must be used in path), such points should be marked as "---"
                
          
         Example of input file:
         -----------------------
         6 6
         r d u l
         0 0
         5 5
         hex
         46B E59 EA --- 45E 63
         899 FFF 926 7AD C4E FFF
         E2E --- 6D2 976 83F C96
         9E9 A8B 9C1 461 F74 D05
         EDD E94 5F4 D1D D03 DE3
         89 925 CF9 CA0 F18 4D2
         ----------------------
         '''
    print(msg)


def read_data(file_name):
    try:
        with open(file_name, 'r') as f:
            trn = Size(*map(int, f.readline().split()))
            mv = map(lambda m: m.lower(), f.readline().split())
            drn = Directions(right='r' in mv, down='d' in mv,
                             up='u' in mv, left='l' in mv)
            start = Point(*map(int, f.readline().split()))
            end = Point(*map(int, f.readline().split()))
            code = {'dec': 10, 'hex': 16}.get(f.readline().strip(), 16)
            matrix = dead = []
            for y in range(trn.Y):
                row_raw = f.readline()
                # use -1 as a special value
                if '-1' in row_raw:
                    print('Error: all the values on matrix must me non negative')
                    print_help()
                    sys.exit(0)
                row = list(map(lambda h: int(h, code), row_raw.replace('---', '-1').split()))
                dead = dead + [Point(x, y) for x, v in enumerate(row) if v == -1]
                matrix.append(row)
            return matrix, start, end, trn, drn, dead, code
    except FileNotFoundError:
        print('ERROR: file with name %s not found' % file_name)
        sys.exit(0)
    except Exception:
        print('''ERROR: file %s has wrong format''' % file_name)
        print_help()
        sys.exit(0)


def print_path(matrix, path, code):
    func = lambda v: hex(v)[2:] if code == 16 else str(v)
    max_len = max([max([len(func(n)) for n in row]) for row in matrix])
    for y, row in enumerate(matrix):
        strng = ''
        for x, v in enumerate(row):
            if v == -1:
                wrapped = ''.join(['-'] * max_len)
            else:
                value = func(v)
                wrapped = value + ''.join([' '] * (max_len - len(value)))
            wrapped2 = '[' + wrapped + ']' if any(map(lambda a: Point(x, y) in a, path)) else ' ' + wrapped + ' '
            strng = strng + wrapped2 + ' '
        print(strng)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('This program requires file name as an argument')
        print('Usage: python %s file_name' % sys.argv[0])
        print_help()
        sys.exit(0)
    matrix, start, end, trn, drn, dead, code = read_data(sys.argv[1])
    validate_input(matrix, start, end, trn, drn, dead, code)
    if start == end:
        print('Start point is equal to end point, no need to move, cost is 0')
        sys.exit(0)

    result = solver(matrix, start, end, trn, drn, dead)
    if not result:
        print('Unable to find feasible solution')
        sys.exit(0)
    path, words, cost = result

    print('Minimum cost is: %s ' % str(cost))
    print('Optimal path is:')
    print_path(matrix, path, code)
    print(' -> '.join(words))



